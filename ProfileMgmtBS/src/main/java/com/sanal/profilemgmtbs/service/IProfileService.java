package com.sanal.profilemgmtbs.service;

import com.sanal.profilemgmtbs.dbo.Profile;

public interface IProfileService {
    public Profile getProfile(int id);
}
