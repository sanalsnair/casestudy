package com.sanal.profilemgmtbs.service.imp;

import com.sanal.profilemgmtbs.datastore.Database;
import com.sanal.profilemgmtbs.dbo.Profile;
import com.sanal.profilemgmtbs.service.IProfileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class ProfileService implements IProfileService {
    @Autowired
    Database database;
    @Value("${cacheurl:http://localhost:8080}")
    String cacheURL;
    @Override
    public Profile getProfile(int id) {
        Profile profile=null;
        log.debug("URL for cache {}",cacheURL);
        //Get data from Cache
        WebClient client = WebClient.create(cacheURL);
        Mono<Profile> result  = client.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/profile/"+id)
                        .build())
                .retrieve()
                .bodyToMono(Profile.class);
        profile=result.block();
        log.debug("Result from cache {}",profile);
        //If not available get data from databse
        if(profile == null){
            profile=database.getFromDB(id);
            log.debug("Profile from database {}",profile);
            //got profile from database
            if(profile != null){
                client = WebClient.create(cacheURL);
                Mono<Profile> result2  = client.post()
                        .uri(uriBuilder -> uriBuilder
                                .path("/profile")
                                .build())
                            .body( Mono.just(profile),Profile.class)
                        .retrieve()
                        .bodyToMono(Profile.class);
                profile=result2.block();
                log.debug("Profile stored in cache {}",profile);
            }
        }
        return profile;
    }
}
