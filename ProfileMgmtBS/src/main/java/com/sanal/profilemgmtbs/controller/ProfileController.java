package com.sanal.profilemgmtbs.controller;

import com.sanal.profilemgmtbs.dbo.Profile;
import com.sanal.profilemgmtbs.service.IProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProfileController {

    @Autowired
    IProfileService iProfileService;
    @GetMapping("/profile/{id}")
    public Profile getProfile(@PathVariable("id") int id){
        return  iProfileService.getProfile(id);
    }
}
