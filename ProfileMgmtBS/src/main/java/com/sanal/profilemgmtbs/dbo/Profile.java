package com.sanal.profilemgmtbs.dbo;

import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Profile {
    int id;
    String name;
    String email;
}
