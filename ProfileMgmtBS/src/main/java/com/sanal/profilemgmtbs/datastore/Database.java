package com.sanal.profilemgmtbs.datastore;

import com.sanal.profilemgmtbs.dbo.Profile;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class Database {
    HashMap<Integer, Profile> profileData  = new HashMap<>();
    public  Profile getFromDB(int id){
        return profileData.get(id);
    }
    public  Profile putIntoDB(Profile p){
        return profileData.put(p.getId(),p);
    }
}
