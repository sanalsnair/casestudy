package com.sanal.profilemgmtbs.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleAllException(Exception ex, WebRequest request) 
	{
		ExceptionResponse exResponse =  
				new ExceptionResponse(ex.getMessage()  );
		log.error("Error for web request:{} ",request);
		log.error("Exception {}",ex);
		return new ResponseEntity(exResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	

}
