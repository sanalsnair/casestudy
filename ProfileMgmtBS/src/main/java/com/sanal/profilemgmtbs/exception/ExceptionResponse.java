package com.sanal.profilemgmtbs.exception;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter 
@Setter 
public class ExceptionResponse {
		private Date timestamp;
		private String message;
		
		public ExceptionResponse() {};
		
		public ExceptionResponse(Date timestamp,  String message) {
			this.timestamp = timestamp;
			this.message = message;
		}
	public ExceptionResponse( String message) {
		this.timestamp = new Date();
		this.message = message;
	}
}
