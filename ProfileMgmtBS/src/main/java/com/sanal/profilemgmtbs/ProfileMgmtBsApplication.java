package com.sanal.profilemgmtbs;

import com.sanal.profilemgmtbs.datastore.Database;
import com.sanal.profilemgmtbs.dbo.Profile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class ProfileMgmtBsApplication implements CommandLineRunner {
    @Autowired
    Database database;
    public static void main(String[] args) {
        SpringApplication.run(
                ProfileMgmtBsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //Populate Data
        Profile profile1 = Profile.builder()
                .id(1)
                .name("Peter")
                .email("peter@test.com")
                .build();
        Profile profile2 = Profile.builder()
                .id(2)
                .name("Sam")
                .email("sam@test.com")
                .build();
        Profile profile3 = Profile.builder()
                .id(3)
                .name("Tracy")
                .email("tracy@test.com")
                .build();
        Profile profile4 = Profile.builder()
                .id(4)
                .name("David")
                .email("david@test.com")
                .build();
        database.putIntoDB(profile1);
        database.putIntoDB(profile2);
        database.putIntoDB(profile3);
        database.putIntoDB(profile4);
        log.debug("Database list {}",database);
    }
}
