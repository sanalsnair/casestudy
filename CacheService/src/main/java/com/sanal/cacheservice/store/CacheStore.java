package com.sanal.cacheservice.store;

import com.sanal.cacheservice.dbo.Profile;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class CacheStore {
    HashMap<Integer, Profile> cache = new HashMap<>();

    public  Profile getFromCache(int id){
        return cache.get(id);
    }
    public  Profile putInCache(Profile p){
        return cache.put(p.getId(),p);
    }

}
