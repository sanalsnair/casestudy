package com.sanal.cacheservice.service;

import com.sanal.cacheservice.dbo.Profile;

public interface ICacheService {
    public Profile getProfile(int id);
    public void putProfile(Profile profile);
}
