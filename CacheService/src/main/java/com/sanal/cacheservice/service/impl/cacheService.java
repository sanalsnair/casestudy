package com.sanal.cacheservice.service.impl;

import com.sanal.cacheservice.dbo.Profile;
import com.sanal.cacheservice.service.ICacheService;
import com.sanal.cacheservice.store.CacheStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class cacheService implements ICacheService {
    @Autowired
    CacheStore cacheStore;

    @Override
    public Profile getProfile(int id) {
        return cacheStore.getFromCache(id);
    }

    @Override
    public void putProfile(Profile profile) {
        cacheStore.putInCache(profile);
    }
}
