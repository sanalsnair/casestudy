package com.sanal.cacheservice.dbo;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Profile {
    int id;
    String name;
    String email;
}
