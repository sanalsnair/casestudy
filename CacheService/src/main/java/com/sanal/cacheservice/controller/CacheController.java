package com.sanal.cacheservice.controller;

import com.sanal.cacheservice.dbo.Profile;
import com.sanal.cacheservice.service.ICacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
public class CacheController {

    @Autowired
    ICacheService iCacheService;
    @GetMapping("/profile/{id}")
    public Profile getProfile(@PathVariable("id") int  id){
        log.debug("Getting profile from cache id: {}",id);
        Profile profile=iCacheService.getProfile(id);
        log.debug("Profile: {}",profile);
        return profile;
    }
    @PostMapping("/profile")
    public Profile postProfile(@RequestBody() Profile profile){
        log.debug("Profile to be added to cache: {}",profile);
        iCacheService.putProfile(profile);
        return profile;
    }
}
